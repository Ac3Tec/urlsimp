function update_status(new_status) {
    document.getElementById("values").innerHTML = new_status
}

function save() {
    update_status("Shortening...")
    request = new XMLHttpRequest()
    request.open("GET", document.location.href+ "SAVE/" + value.value, true)
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            update_status(document.location.href+request.response)
            document.getElementById("values").value = document.location.href + request.response;
            document.getElementById("values").select();
            document.getElementById('myBtn').click()
        }
    }
    request.send()
}

var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
    modal.style.display = "block";
}
span.onclick = function() {
    modal.style.display = "none";
}
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function throttle(func, delay) {
  let isRunning;
  return function(...args) {
    let context = this;        // store the context of the object that owns this function
    if(!isRunning) {
      isRunning = true;
      func.apply(context,args) // execute the function with the context of the object that owns it
      setTimeout(function() {
        isRunning = false;
      }, delay);
    }
  }
}

function myFunc(param) {
    var copyText = document.getElementById("values");
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/
    document.execCommand("copy");
    $('<div style="font-weight: bold; color: green; position: absolute; text-align: center; margin-left: auto; margin-right: auto; left: 0; right: 0; margin-top: 10px;">Copied!</div>').insertAfter('#values').delay(1000).fadeOut();
}

let obj = {
  name: "THROTTLED FUNCTION ",
  throttleFunc: throttle(myFunc, 1000)
}

function handleClick() {
  obj.throttleFunc(new Date().getSeconds());
}
